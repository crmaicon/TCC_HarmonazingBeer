# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-06-03 17:14
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('cloud', '0013_nota_preocessed'),
    ]

    operations = [
        migrations.CreateModel(
            name='MediaNotas',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('alcool', models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True)),
                ('ibu', models.IntegerField(blank=True, null=True)),
                ('nota', models.DecimalField(blank=True, decimal_places=2, max_digits=5, null=True)),
                ('cidade', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='cloud.Cidade')),
                ('comida', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='cloud.Comida')),
                ('cor', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='cloud.Cor')),
                ('estado', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='cloud.Estado')),
                ('fabricante', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='cloud.Fabricante')),
                ('pais', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='pais', to='cloud.Pais')),
                ('paisOrigem', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, related_name='paisOrigem', to='cloud.Pais')),
                ('prato', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='cloud.Prato')),
                ('regiao', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='cloud.Regiao')),
                ('tipo', models.ForeignKey(null=True, on_delete=django.db.models.deletion.CASCADE, to='cloud.Tipo')),
            ],
        ),
    ]
