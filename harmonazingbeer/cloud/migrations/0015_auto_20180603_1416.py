# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-06-03 17:16
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('cloud', '0014_medianotas'),
    ]

    operations = [
        migrations.AlterField(
            model_name='medianotas',
            name='nota',
            field=models.DecimalField(decimal_places=2, max_digits=5),
        ),
    ]
