# -*- coding: utf-8 -*-
# Generated by Django 1.11.7 on 2018-10-12 18:03
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('cloud', '0021_auto_20180909_0830'),
    ]

    operations = [
        migrations.RenameField(
            model_name='nota',
            old_name='preocessed',
            new_name='processed',
        ),
    ]
