from django.shortcuts import get_object_or_404
from ..models.model_Beer import Rotulo
from ..models.model_Harmonize import Harmonizacao, Nota, MediaNotas, Sugestao
from ..models.model_Food import Prato, Comida, Acompanhamento
from ..models.model_Person import Usuario
from ..models.model_Address import Estacao, Cidade
from django.db.models import Avg
from django.db import connection

class SugestaoRotulo(object):
    @staticmethod
    def dictfetchall(cursor):
        "Return all rows from a cursor as a dict"
        columns = [col[0] for col in cursor.description]
        return [
            dict(zip(columns, row))
            for row in cursor.fetchall()
        ]
    
    @staticmethod
    def retornarSugestao(request):
        "Não controladas"
        estacao = Estacao.objects.order_by('-id')[0]
        prato = get_object_or_404(Prato, id=request.POST['prato'])        
        usuario = get_object_or_404(Usuario, user=request.user.id)
        cidade = get_object_or_404(Cidade, id=request.POST['cidade_Harmonize'])        
        inf_Local=[]
        inf_Local.append('geral')
        inf_Local.append(1)        
        if (request.POST['nivel'] == '2'):
            inf_Local[0] = 'cidade_id'
            inf_Local[1] = cidade.id
        elif(request.POST['nivel'] == '3'):
            inf_Local[0] = 'estado_id'
            inf_Local[1] = cidade.estado.id
        elif(request.POST['nivel'] == '4'):
            inf_Local[0] = 'pais_id'
            inf_Local[1] = cidade.estado.regiao.pais.id
               
        "Filtros rotulos"
        filtros_Rotulos = {}
        filtros_Rotulos['pais'] = request.POST['pais']
        filtros_Rotulos['fabricante'] = request.POST['fabricante']
        filtros_Rotulos['tipo'] = request.POST['tipo']
        filtros_Rotulos['IBU1'] = request.POST['IBU1']
        filtros_Rotulos['IBU2'] = request.POST['IBU2']
        filtros_Rotulos['TeorAlcoolico1'] = request.POST['TeorAlcoolico1']
        filtros_Rotulos['TeorAlcoolico2'] = request.POST['TeorAlcoolico2']

        try:
            filtros_Rotulos['cbCartaCerveja'] = request.POST['cbCartaCerveja']
        except Exception as identifier:
            filtros_Rotulos['cbCartaCerveja'] = "off"


        lstHarmonizacao =[]
        rows = SugestaoRotulo.retornarRow(request.user.id, request.POST['prato'], inf_Local,filtros_Rotulos)
        for row in rows:
            harmonizacao = Harmonizacao()
            harmonizacao.prato = prato
            harmonizacao.rotulo = get_object_or_404(Rotulo, id=row['id'])
            harmonizacao.usuario = usuario
            harmonizacao.estacao = estacao
            harmonizacao.pontuacao = row['Total']
            lstHarmonizacao.append(harmonizacao)           
        return lstHarmonizacao

    @staticmethod
    def retornarRow(user_id, id_Prato, inf_Local, filtros_Rotulos):
        sQuery = "select rot.ID id, ((select media01.nota * peso.tipo from cloud_medianotas media01 where media01.{} = {} and media01.prato_id = {} and media01.tipo_id = rot.tipo_id) + ".format(inf_Local[0],inf_Local[1], id_Prato)
        sQuery += "(select AVG(media01.nota) * peso.alcool from cloud_medianotas media01 where media01.{} = {} and media01.prato_id = {} and media01.alcool >= (rot.teorAlcoolico - peso.variacaoAlcool) and media01.alcool <= (rot.teorAlcoolico + peso.variacaoAlcool)) + ".format(inf_Local[0],inf_Local[1], id_Prato)
        sQuery += "(select AVG(media01.nota) * peso.ibu from cloud_medianotas media01 where media01.{} = {} and media01.prato_id = {} and media01.ibu >= (rot.ibu - peso.variacaoIBU) and media01.ibu <= (rot.ibu + peso.variacaoIBU)) + ".format(inf_Local[0],inf_Local[1], id_Prato)
        sQuery += "(select media01.nota * peso.cor from cloud_medianotas media01 where media01.{} = {} and media01.prato_id = {} and media01.cor_id = rot.cor_id) + ".format(inf_Local[0],inf_Local[1], id_Prato)
        sQuery += "(select media01.nota * peso.fabricante from cloud_medianotas media01 where media01.{} = {} and media01.prato_id = {} and media01.fabricante_id = rot.fabricante_id) + ".format(inf_Local[0],inf_Local[1], id_Prato)
        sQuery += "(select media01.nota * peso.paisOrigem from cloud_medianotas media01 where media01.{} = {} and media01.prato_id = {} and media01.paisOrigem_id = reg.pais_id)) as Total ".format(inf_Local[0],inf_Local[1], id_Prato)
        sQuery += "from cloud_rotulo as rot "
        sQuery += "inner join cloud_cidade on cloud_cidade.id = rot.cidadeFabricada_id "
        sQuery += "inner join cloud_estado on cloud_estado.id = cloud_cidade.estado_id "
        sQuery += "inner join cloud_regiao as reg on reg.id = cloud_estado.regiao_id "
        sQuery += "inner join cloud_pesocaracteristicas as peso on peso.id = 1 "

        if (filtros_Rotulos['cbCartaCerveja'] == 'on'):
            sQuery += "inner join cloud_carta_cerveja as carta on carta.rotulo_id = rot.id "
            sQuery += "inner join cloud_estabelecimento as estab on estab.pessoa_ptr_id = carta.estabelecimento_id and estab.user_id = {} ".format(user_id)

        sQuery += "where rot.ibu >= {} and rot.ibu <= {} and rot.teorAlcoolico >= {} and rot.teorAlcoolico <= {} ".format(filtros_Rotulos['IBU1'],filtros_Rotulos['IBU2'],filtros_Rotulos['TeorAlcoolico1'],filtros_Rotulos['TeorAlcoolico2'])

        if (filtros_Rotulos['pais'] != '-1'):
            sQuery += "and reg.pais_id = {} ".format(filtros_Rotulos['pais'])

        if (filtros_Rotulos['fabricante'] != '-1'):
            sQuery += "and rot.fabricante_id = {} ".format(filtros_Rotulos['fabricante'])

        if (filtros_Rotulos['tipo'] != '-1'):
            sQuery += "and rot.tipo_id = {} ".format(filtros_Rotulos['tipo'])

        sQuery += "order by ((select media01.nota * peso.tipo from cloud_medianotas media01 where media01.{} = {} and media01.prato_id = {} and media01.tipo_id = rot.tipo_id) + ".format(inf_Local[0],inf_Local[1], id_Prato)
        sQuery += "(select AVG(media01.nota) * peso.alcool from cloud_medianotas media01 where media01.{} = {} and media01.prato_id = {} and media01.alcool >= (rot.teorAlcoolico - peso.variacaoAlcool) and media01.alcool <= (rot.teorAlcoolico + peso.variacaoAlcool)) + ".format(inf_Local[0],inf_Local[1], id_Prato)
        sQuery += "(select AVG(media01.nota) * peso.ibu from cloud_medianotas media01 where media01.{} = {} and media01.prato_id = {} and media01.ibu >= (rot.ibu - peso.variacaoIBU) and media01.ibu <= (rot.ibu + peso.variacaoIBU)) + ".format(inf_Local[0],inf_Local[1], id_Prato)
        sQuery += "(select media01.nota * peso.cor from cloud_medianotas media01 where media01.{} = {} and media01.prato_id = {} and media01.cor_id = rot.cor_id) + ".format(inf_Local[0],inf_Local[1], id_Prato)
        sQuery += "(select media01.nota * peso.fabricante from cloud_medianotas media01 where media01.{} = {} and media01.prato_id = {} and media01.fabricante_id = rot.fabricante_id) + ".format(inf_Local[0],inf_Local[1], id_Prato)
        sQuery += "(select media01.nota * peso.paisOrigem from cloud_medianotas media01 where media01.{} = {} and media01.prato_id = {} and media01.paisOrigem_id = reg.pais_id)) desc ".format(inf_Local[0],inf_Local[1], id_Prato)        
        sQuery += " limit 10"
                
        with connection.cursor() as cursor:
            cursor.execute(sQuery)
            rows = SugestaoRotulo.dictfetchall(cursor)
            return rows        

    @staticmethod
    def retonarMedia(combinacao, nota):
        filtro = {}
        lstMedias ={}
        if(combinacao[0] == 'cidade'):
            filtro['sugestao__usuario__cidade'] = nota.sugestao.usuario.cidade
        elif(combinacao[0] == 'estado'):
            filtro['sugestao__usuario__cidade__estado'] = nota.sugestao.usuario.cidade.estado
        elif(combinacao[0] == 'estado'):
            filtro['sugestao__usuario__cidade__estado__regiao'] = nota.sugestao.usuario.cidade.estado.regiao
        elif(combinacao[0] == 'pais'):
            filtro['sugestao__usuario__cidade__estado__regiao__pais'] = nota.sugestao.usuario.cidade.estado.regiao.pais        
        
        if(combinacao[1] == 'tipo'):
            filtro['sugestao__rotulo__tipo'] = nota.sugestao.rotulo.tipo
        elif(combinacao[1] == 'cor'):
            filtro['sugestao__rotulo__cor'] = nota.sugestao.rotulo.cor
        elif(combinacao[1] == 'teorAlcoolico'):
            filtro['sugestao__rotulo__teorAlcoolico'] = nota.sugestao.rotulo.teorAlcoolico
        elif(combinacao[1] == 'ibu'):
            filtro['sugestao__rotulo__ibu'] = nota.sugestao.rotulo.ibu        
        elif(combinacao[1] == 'fabricante'):
            filtro['sugestao__rotulo__fabricante'] = nota.sugestao.rotulo.fabricante
        elif(combinacao[1] == 'paisOrigem'):
            filtro['sugestao__rotulo__cidadeFabricada__estado__regiao__pais'] = nota.sugestao.rotulo.cidadeFabricada.estado.regiao.pais
        
        if(combinacao[2]== 'prato'):
            filtro['sugestao__prato'] = nota.sugestao.prato
            lstMedias[0] = Nota.objects.filter(**filtro).aggregate(Avg('nota'))["nota__avg"]
        elif(combinacao[2] == 'comida'):            
            acompanhamentosComida = Acompanhamento.objects.filter(prato=nota.sugestao.prato)            
            for acompanha in acompanhamentosComida:
                acompanhamentosPrato = Acompanhamento.objects.filter(comida=acompanha.comida)
                somaNotas = 0
                for acompanhaPrato in acompanhamentosPrato:
                    filtro['sugestao__prato']=acompanhaPrato.prato
                    somaNotas = somaNotas + Nota.objects.filter(**filtro).aggregate(Avg('nota'))["nota__avg"]
                medianota = somaNotas/len(acompanhamentosPrato)
                lstMedias[acompanha.comida] = medianota                        
        return lstMedias

    @staticmethod
    def retonarListaCombinacoes():
        lstlocal=["cidade","estado", "regiao", "pais", "geral"]
        lstrotulo =["tipo", "teorAlcoolico", "ibu", "cor", "fabricante", "paisOrigem"]
        lstFood = ["comida", "prato"]
        lstCombinacoes =[]
        for local in lstlocal:
            for rotulo in lstrotulo:
                for food in lstFood:
                    lstCombinacoes.append([local, rotulo, food])
        return lstCombinacoes
    
    @staticmethod
    def gravarNovaMedia(nota, combinacao, notaMedia, comida):
        mediaNota = MediaNotas()
        mediaNota.nota = notaMedia
        if(combinacao[0] == 'cidade'):
            mediaNota.cidade = nota.sugestao.usuario.cidade
        elif(combinacao[0] == 'estado'):
            mediaNota.estado = nota.sugestao.usuario.cidade.estado
        elif(combinacao[0] == 'estado'):
            mediaNota.regiao = nota.sugestao.usuario.cidade.estado.regiao
        elif(combinacao[0] == 'pais'):
            mediaNota.pais = nota.sugestao.usuario.cidade.estado.regiao.pais
        elif(combinacao[0] == 'geral'):
            mediaNota.geral = 1

        if(combinacao[1] == 'tipo'):
            mediaNota.tipo = nota.sugestao.rotulo.tipo
        elif(combinacao[1] == 'cor'):
            mediaNota.cor = nota.sugestao.rotulo.cor
        elif(combinacao[1] == 'teorAlcoolico'):
            mediaNota.alcool = nota.sugestao.rotulo.teorAlcoolico
        elif(combinacao[1] == 'ibu'):
            mediaNota.ibu = nota.sugestao.rotulo.ibu        
        elif(combinacao[1] == 'fabricante'):
            mediaNota.fabricante = nota.sugestao.rotulo.fabricante
        elif(combinacao[1] == 'paisOrigem'):
            mediaNota.paisOrigem = nota.sugestao.rotulo.cidadeFabricada.estado.regiao.pais
        
        if(combinacao[2]== 'prato'):
            mediaNota.prato = nota.sugestao.prato      
        elif(combinacao[2]== 'comida'):
            mediaNota.comida = comida
        mediaNota.save()

    @staticmethod
    def gravarMedia(mediaComb, combinacao, nota):
        filtro = {}
        if(combinacao[0] == 'cidade'):
            filtro['cidade'] = nota.sugestao.usuario.cidade
        elif(combinacao[0] == 'estado'):
            filtro['estado'] = nota.sugestao.usuario.cidade.estado
        elif(combinacao[0] == 'estado'):
            filtro['regiao'] = nota.sugestao.usuario.cidade.estado.regiao
        elif(combinacao[0] == 'pais'):
            filtro['pais'] = nota.sugestao.usuario.cidade.estado.regiao.pais
        elif(combinacao[0] == 'geral'):
            filtro['geral'] = 1

        if(combinacao[1] == 'tipo'):
            filtro['tipo'] = nota.sugestao.rotulo.tipo
        elif(combinacao[1] == 'cor'):
            filtro['cor'] = nota.sugestao.rotulo.cor
        elif(combinacao[1] == 'teorAlcoolico'):
            filtro['alcool'] = nota.sugestao.rotulo.teorAlcoolico
        elif(combinacao[1] == 'ibu'):
            filtro['ibu'] = nota.sugestao.rotulo.ibu        
        elif(combinacao[1] == 'fabricante'):
            filtro['fabricante'] = nota.sugestao.rotulo.fabricante
        elif(combinacao[1] == 'paisOrigem'):
            filtro['paisOrigem'] = nota.sugestao.rotulo.cidadeFabricada.estado.regiao.pais
        
        if(combinacao[2] == 'prato'):
            filtro['prato'] = nota.sugestao.prato
            mediaNota = MediaNotas.objects.filter(**filtro)
            if(len(mediaNota) > 0):
                mediaNota[0].nota = mediaComb[0]
                mediaNota[0].save()
            else:
                SugestaoRotulo.gravarNovaMedia(nota, combinacao, mediaComb[0], 1)
        elif (combinacao[2] == 'comida'):
            for comida in mediaComb.keys():
                filtro['comida'] = comida
                mediaNota = MediaNotas.objects.filter(**filtro)
                if(len(mediaNota) > 0):
                    mediaNota[0].nota = mediaComb[comida]
                    mediaNota[0].save()
                else:
                    SugestaoRotulo.gravarNovaMedia(nota, combinacao, mediaComb[comida], comida)
                    
    @staticmethod
    def gerarMediaNotas():
        lstNotasNaoProcessadas = Nota.objects.filter(processed=False,nota__isnull=False)
        lstCombinacoes = SugestaoRotulo.retonarListaCombinacoes()
        for nota in lstNotasNaoProcessadas:
            for combinacao in lstCombinacoes:
                mediaComb = SugestaoRotulo.retonarMedia(combinacao, nota)                   
                "print(combinacao[0] +' - '+combinacao[1] +' - '+ combinacao[2])"
                try:
                    SugestaoRotulo.gravarMedia(mediaComb, combinacao, nota)
                    nota.preocessed = True
                    nota.save()
                    "print(mediaComb)"
                except Exception as identifier:
                    print(identifier.args[0])
    
    @staticmethod
    def gerarNotaCadastroRotulo(rotulo, request):
        lstPratos = Prato.objects.order_by('-id')
        for prato in lstPratos:
            sugestao = Sugestao()
            sugestao.rotulo = rotulo
            sugestao.prato = prato
            sugestao.usuario = get_object_or_404(Usuario, user=request.user.id)        
            sugestao.estacao = Estacao.objects.order_by('-id')[0]        
            sugestao.save()
            nota = Nota()
            nota.nota = 5.0
            nota.sugestao = sugestao
            nota.save()
        SugestaoRotulo.gerarMediaNotas()
    
    @staticmethod
    def gerarNotaCadastroPrato(prato, request):
        lstRotulos = Rotulo.objects.order_by('-id')
        for rotulo in lstRotulos:
            sugestao = Sugestao()
            sugestao.rotulo = rotulo
            sugestao.prato = prato
            sugestao.usuario = get_object_or_404(Usuario, user=request.user.id)        
            sugestao.estacao = Estacao.objects.order_by('-id')[0]      
            sugestao.save()
            nota = Nota()
            nota.nota = 5.0
            nota.sugestao = sugestao
            nota.save()
        SugestaoRotulo.gerarMediaNotas()