from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone

from .models.model_Beer import Cor, Tipo

class IndexView(generic.ListView):
    template_name = 'cloud/index.html'
    def get_queryset(self):
        return ''
   
class AboutView(generic.ListView):   
    template_name = 'cloud/about.html'
    def get_queryset(self):
        return ''

class TipoView(generic.ListView):
    model = Tipo
    template_name = 'cloud/cadTipo.html'
    context_object_name = 'latest_tipo_list'

    def get_queryset(self):
        return Tipo.objects.order_by('-nome')[:5]

    def get_context_data(self, *args, **kwargs):
        context = super(TipoView, self).get_context_data(*args, **kwargs)
        context['latest_cor_list'] = Cor.objects.order_by('nome')    
        return context 

def cadastrarTipo(request):
    try:
        tipo = Tipo()
        tipo.nome = request.POST['nome']
        tipo.cor = get_object_or_404(Cor, pk=request.POST['cor'])
        tipo.save()
    except:
        return HttpResponseRedirect(reverse('cloud:tipo'))
    else:
        return HttpResponseRedirect(reverse('cloud:tipo'))

class CorView(generic.ListView):
    model = Cor
    template_name = 'cloud/cadCor.html'
    context_object_name = 'latest_cor_list'

    def get_queryset(self):
        return Cor.objects.order_by('-id')[:30]

def cadastrarCor(request):
    try:
        cor = Cor()
        cor.nome = request.POST['nome']
        cor.save()
    except:
        return HttpResponseRedirect(reverse('cloud:cor'))
    else:
        return HttpResponseRedirect(reverse('cloud:cor'))
