from django.db import models
from .model_Address import *
from .model_Person import *

#Classe cor
class Cor (models.Model):
    nome = models.CharField(max_length=50)    
    class Meta:
        app_label = 'cloud' 

#Classe Tipo
class Tipo (models.Model):
    nome = models.CharField(max_length=50)
    cor = models.ForeignKey(Cor, on_delete=models.CASCADE)
    class Meta:
        app_label = 'cloud'

#Classe Rotulo
class Rotulo (models.Model):
    nome = models.CharField(max_length=50)
    descricao = models.CharField(max_length=200)
    ibu = models.IntegerField()
    teorAlcoolico = models.DecimalField(max_digits=4, decimal_places=2)    
    fabricante = models.ForeignKey(Fabricante, on_delete=models.CASCADE)
    cidadeFabricada = models.ForeignKey(Cidade, on_delete=models.CASCADE)
    tipo = models.ForeignKey(Tipo, on_delete=models.CASCADE)
    cor = models.ForeignKey(Cor, on_delete=models.CASCADE)
    class Meta:
        app_label = 'cloud' 


#Classe Carta_Cerveja
class Carta_Cerveja (models.Model):    
    estabelecimento = models.ForeignKey(Estabelecimento, on_delete=models.CASCADE)
    rotulo = models.ForeignKey(Rotulo, on_delete=models.CASCADE)
    class Meta:
        app_label = 'cloud' 