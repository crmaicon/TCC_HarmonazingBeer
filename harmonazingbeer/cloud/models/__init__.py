from django.db import models

from .model_Address import *
from .model_Person import *
from .model_Beer import *
from .model_Food import *
from .model_Harmonize import *