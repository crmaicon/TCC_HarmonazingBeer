from django.db import models
from .model_Beer import *

#Classe comida
class Comida (models.Model):
    nome = models.CharField(max_length=50)   
    descricao = models.CharField(max_length=200)   
    class Meta:
        app_label = 'cloud' 

# Classe Prato
class Prato (models.Model):
    nome = models.CharField(max_length=50)   
    descricao = models.CharField(max_length=200)
    comidaPrincipal = models.ForeignKey(Comida, on_delete=models.CASCADE)    
    class Meta:
        app_label = 'cloud'

#Classe Acompanhamento
class Acompanhamento (models.Model):
    comida = models.ForeignKey(Comida, on_delete=models.CASCADE)
    prato = models.ForeignKey(Prato, on_delete=models.CASCADE)
    class Meta:
        app_label = 'cloud'