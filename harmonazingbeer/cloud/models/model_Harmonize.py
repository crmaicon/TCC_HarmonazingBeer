from django.db import models
from .model_Beer import *
from .model_Food import *
from .model_Person import *
from .model_Address import *

# Classe Harmonizacao
class Harmonizacao (models.Model):    
    rotulo = models.ForeignKey(Rotulo, on_delete=models.CASCADE)    
    prato = models.ForeignKey(Prato, on_delete=models.CASCADE)    
    usuario = models.ForeignKey(Usuario, on_delete=models.CASCADE)    
    estacao = models.ForeignKey(Estacao, on_delete=models.CASCADE)
    pontuacao = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    class Meta:
        app_label = 'cloud'

# Classe Sugestao
class Sugestao (Harmonizacao):    
    data = models.DateField(auto_now=True)  

# Classe nota
class Nota (models.Model):    
    sugestao = models.ForeignKey(Sugestao, on_delete=models.CASCADE)
    nota = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    processed = models.BooleanField(default=False)
    class Meta:
        app_label = 'cloud'

class MediaNotas(models.Model):
    cidade = models.ForeignKey(Cidade, on_delete=models.CASCADE, null=True)
    estado = models.ForeignKey(Estado, on_delete=models.CASCADE, null=True)
    regiao = models.ForeignKey(Regiao, on_delete=models.CASCADE, null=True)
    pais = models.ForeignKey(Pais, on_delete=models.CASCADE, null=True, related_name='pais')
    tipo =models.ForeignKey(Tipo, on_delete=models.CASCADE, null=True)
    alcool = models.DecimalField(max_digits=5, decimal_places=2, blank=True, null=True)
    ibu = models.IntegerField(blank=True, null=True)
    cor = models.ForeignKey(Cor, on_delete=models.CASCADE, null=True)
    fabricante = models.ForeignKey(Fabricante, on_delete=models.CASCADE, null=True)
    paisOrigem = models.ForeignKey(Pais, on_delete=models.CASCADE, null=True, related_name='paisOrigem')
    comida = models.ForeignKey(Comida, on_delete=models.CASCADE, null=True)
    prato = models.ForeignKey(Prato, on_delete=models.CASCADE, null=True)
    geral = models.IntegerField(blank=True, null=True)
    nota =models.DecimalField(max_digits=10, decimal_places=5)

class PesoCaracteristicas(models.Model):
    tipo = models.DecimalField(max_digits=5,decimal_places=2)
    alcool = models.DecimalField(max_digits=5,decimal_places=2)
    ibu = models.DecimalField(max_digits=5,decimal_places=2)
    cor = models.DecimalField(max_digits=5,decimal_places=2)
    fabricante = models.DecimalField(max_digits=5,decimal_places=2)
    paisOrigem = models.DecimalField(max_digits=5,decimal_places=2)
    comida = models.DecimalField(max_digits=5,decimal_places=2)
    variacaoAlcool= models.DecimalField(max_digits=5,decimal_places=2)
    variacaoIBU = models.DecimalField(max_digits=5,decimal_places=2)