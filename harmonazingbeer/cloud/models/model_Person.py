from django.db import models
from .model_Address import *
from django.contrib.auth.models import User

#Model Pessoa
class Pessoa (models.Model):
    nome = models.CharField(max_length=100)
    cidade = models.ForeignKey(Cidade, on_delete=models.CASCADE)
    class Meta:
        app_label = 'cloud'  

class Fabricante(Pessoa):
    def criarclasse(self):
        return 1

class Usuario(Pessoa):
    #Ligação com o user Geral
    user = models.ForeignKey(User, blank=True, null=True, editable=False)
    codUsuario = models.CharField(max_length=20)
    email = models.EmailField(max_length=20)
    senha = models.CharField(max_length=10)
    maitre = models.BooleanField()    

    def save(self):
        erro = True
        if not self.id:
            c = Usuario.objects.filter(codUsuario=self.codUsuario).count()
            if (c==0):
                erro = False

            if (not erro):
                usr = User.objects.filter(username=self.codUsuario)

                if usr:
                    u = usr[0]
                else:
                    u = User.objects.create_user(self.codUsuario, self.email, self.senha)
                u.save()
                self.user = u
        else:
            self.user.username = self.codUsuario
            self.user.email = self.email
            self.user.set_password(self.senha)
        
        if (not erro):
            super(Usuario, self).save()

    def __unicode__(self):
        return self.nome

class Estabelecimento(Pessoa):
    user = models.ForeignKey(Usuario, on_delete=models.CASCADE) 