from django.db import models

#Model Continente 
class Continente(models.Model):    
    nome = models.CharField(max_length=50)
    def __str__(self):
        return self.nome

    def PopularBanco(self):
        continentes =['Africa','América do Sul','America do Norte', 'América Central', 'Europa', 'Ásia', 'Oceania']
        for nomeCont in continentes:
            co = Continente()
            co.nome = nomeCont
            co.save()        

    class Meta:
        app_label = 'cloud'

#Model Pais
class Pais (models.Model):
    nome = models.CharField(max_length=50)
    continente = models.ForeignKey(Continente, on_delete=models.CASCADE)
    class Meta:
        app_label = 'cloud'    

#Model Clima
class Clima (models.Model):
    nome = models.CharField(max_length=50)
    descricao = models.CharField(max_length=200)

    def PopularBanco(self):
        climas =[
            ['Equatorial', 'Possui temperaturas médias acima de 25°C; clima quente e úmido, com índices pluviométricos anuais acima de 2000 mm'],
            ['Tropical', 'Temperatura variando em 20°C no inverno e 25°C no verão, com duas estações bem definidas, uma seca e outra chuvosa'],
            ['Subtropical', 'Possui temperaturas médias entre 15°C e 20°C no verão, e no inverno as médias variam entre 0°C a 10°C, chuvas bem distribuídas'],
            ['Oceânico', 'Recebe influência dos oceanos e mares, tornando os invernos menos rigorosos'],
            ['Continental', 'Praticamente não recebe influência dos oceanos, possui um inverno mais rigoroso'],
            ['Mediterrâneo','Possui invernos chuvosos e verões quentes, com quatro estações bem definidas'],
            ['Desértico','Temperaturas médias anuais variam entre 20°C e 30°C, os índices de precipitações não ultrapassam 250 mm ao ano'],
            ['Semiárido','Caracterizado por altas temperaturas podendo chegar a 32°C, os índices pluviométricos são inferiores a 600 mm anuais e as chuvas são irregulares'],
            ['Frio de montanha', 'Temperatura determinada pela altitude, quanto mais alto mais frio, mesmo em regiões tropicais'],
            ['Polar','Caracteriza-se por longos invernos e verões secos e curtos, temperaturas anuais sempre abaixo de zero, e marcante presença de neve e gelo']
        ]
        for cl in climas:
            clima = Clima()
            clima.nome = cl[0]
            clima.descricao = cl[1]
            clima.save() 

    class Meta:
        app_label = 'cloud'

#Model Estacao
class Estacao (models.Model):
    nome = models.CharField(max_length=50) 

    def PopularBanco(self):
        estacoes =['Verão','Outono','Primavera', 'Verão']
        for est in estacoes:
            es = Estacao()
            es.nome = est
            es.save()    

    class Meta:
        app_label = 'cloud'

#Model Regiao
class Regiao (models.Model):
    nome = models.CharField(max_length=50)
    pais = models.ForeignKey(Pais, on_delete=models.CASCADE)
    clima = models.ForeignKey(Clima, on_delete=models.CASCADE)
    class Meta:
        app_label = 'cloud'

#Model Estado
class Estado (models.Model):
    nome = models.CharField(max_length=50)
    regiao = models.ForeignKey(Regiao, on_delete=models.CASCADE)
    clima = models.ForeignKey(Clima, on_delete=models.CASCADE)
    class Meta:
        app_label = 'cloud'

#Model Cidade
class Cidade (models.Model):
    nome = models.CharField(max_length=50)
    estado = models.ForeignKey(Estado, on_delete=models.CASCADE)
    clima = models.ForeignKey(Clima, on_delete=models.CASCADE)
    class Meta:
        app_label = 'cloud'