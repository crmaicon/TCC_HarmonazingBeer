from django.conf.urls import url
from . import views

app_name = 'cloud'
urlpatterns = [
    #1 - Views de Cadastros
    url(r'^$', views.IndexView.as_view(), name='index'),
    url(r'^beer$', views.IndexOnView, name='indexOn'),
    url(r'^about/$', views.AboutView.as_view(), name='about'),
    #1.1 Address
    url(r'^pais/$', views.PaisView, name='pais'),
    url(r'^regiao/$', views.RegiaoView, name='regiao'),
    url(r'^estado/$', views.EstadoView, name='estado'),
    url(r'^cidade/$', views.CidadeView, name='cidade'),
    #1.2 Beer
    url(r'^tipo/$', views.TipoView, name='tipo'),
    url(r'^cor/$', views.CorView, name='cor'),
    url(r'^rotulo/$', views.RotuloView, name='rotulo'),
    #1.3 Person
    url(r'^usuario/$', views.UsuarioView, name='usuario'),
    url(r'^usuarioNew/$', views.UsuarioNewView.as_view(), name='usuarioNew'),
    url(r'^fabricante/$', views.FabricanteView, name='fabricante'),
    url(r'^estabelecimento/$', views.EstabelecimentoView, name='estabelecimento'),
    #1.4 Food
    url(r'^comida/$', views.ComidaView, name='comida'),
    url(r'^prato/$', views.PratoView, name='prato'),
    url(r'^acompanhamento/$', views.AcompanhamentoView, name='acompanhamento'),
    #1.5 Harmonize
    url(r'^Sugestao/$', views.sugestaoView, name='sugestao'),
    url(r'^Nota/$', views.notaView, name='nota'),
    url(r'^Peso/$', views.pesoView, name='peso'),

    #2 - Views de ações
    url(r'^logar/$', views.login, name='logar'),
    url(r'^deslogar/$', views.sair, name='sair'),
    #2.1 Address
    url(r'^cadastrarPais/$', views.cadastrarPais, name='cadastrarPais'),
    url(r'^cadastrarRegiao/$', views.cadastrarRegiao, name='cadastrarRegiao'),
    url(r'^cadastrarEstado/$', views.cadastrarEstado, name='cadastrarEstado'),
    url(r'^cadastrarCidade/$', views.cadastrarCidade, name='cadastrarCidade'),
    #2.1.1 deletes
    url(r'^deletePais/$', views.toDeletePais, name='deletePais'),
    url(r'^deleteRegiao/$', views.toDeleteRegiao, name='deleteRegiao'),
    url(r'^deleteEstado/$', views.toDeleteEstado, name='deleteEstado'),
    url(r'^deleteCidade/$', views.toDeleteCidade, name='deleteCidade'),
    #2.2 Beer
    url(r'^cadastrarCor/$', views.cadastrarCor, name='cadastrarCor'),
    url(r'^cadastrarTipo/$', views.cadastrarTipo, name='cadastrarTipo'),
    url(r'^cadastrarRotulo/$', views.cadastrarRotulo, name='cadastrarRotulo'),
    url(r'^cadastrarCartaCerveja/$', views.cadastrarCartaCerveja, name='cadastrarCartaCerveja'),
    #2.2.1 deletes
    url(r'^deleteCor/$', views.toDeleteCor, name='deleteCor'),
    url(r'^deleteTipo/$', views.toDeleteTipo, name='deleteTipo'),
    url(r'^deleteRotulo/$', views.toDeleteRotulo, name='deleteRotulo'),
    #url(r'^deleteCartaCerveja/$', views.toDeleteCarta, name='deleteCartaCerveja'),
    #2.3 Person
    url(r'^cadastrarUsuario/$', views.cadastrarUsuario, name='cadastrarUsuario'),
    url(r'^cadastrarFabricante/$', views.cadastrarFabricante, name='cadastrarFabricante'),
    url(r'^cadastrarEstabelecimento/$', views.cadastrarEstabelecimento, name='cadastrarEstabelecimento'),
    #2.3.1 Deletes
    url(r'^deleteUsuario/$', views.toDeleteUsuario, name='deleteUsuario'),
    url(r'^deleteFabricante/$', views.toDeleteFabricante, name='deleteFabricante'),
    url(r'^deleteEstabelecimento/$', views.toDeleteEstabelecimento, name='deleteEstabelecimento'),
    #2.4 Food
    url(r'^cadastrarComida/$', views.cadastrarComida, name='cadastrarComida'),
    url(r'^cadastrarPrato/$', views.cadastrarPrato, name='cadastrarPrato'),
    url(r'^cadastrarAcompanhamento/$', views.cadastrarAcompanhamento, name='cadastrarAcompanhamento'),    
    #2.4.1 Deletes
    url(r'^deleteComida/$', views.toDeleteComida, name='deleteComida'),
    url(r'^deletePrato/$', views.toDeletePrato, name='deletePrato'),
    url(r'^deleteAcompanhamento/$', views.toDeleteAcompanhamento, name='deleteAcompanhamento'),
    #2.5 Harmonize
    url(r'^gerarSugestao/$', views.gerarSugestao, name='gerarSugestao'),
    url(r'^gravarRotuloSelecionado/$', views.gravarRotuloSelecionado, name='gravarRotuloSelecionado'),
    url(r'^gravarNota/$', views.gravarNota, name='gravarNota'),
    url(r'^gravarPeso/$', views.gravarPeso, name='gravarPeso'),
]
