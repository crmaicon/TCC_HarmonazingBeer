from django.shortcuts import get_object_or_404, render, render_to_response
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.template import loader
from django.contrib.auth.decorators import login_required
from ..models.model_Address import *

# Cadastro de Pais
@login_required
def PaisView(request):
    latest_pais_list = Pais.objects.order_by('-nome')[:30]
    latest_continente_list = Continente.objects.order_by('nome')

    if len(latest_continente_list) < 1:
        Continente.PopularBanco()
        latest_continente_list = Continente.objects.order_by('nome')

    template = loader.get_template('cloud/cadPais.html')
    context = {
        'latest_pais_list':latest_pais_list,
        'latest_continente_list':latest_continente_list,
    }
    return HttpResponse(template.render(context, request))

@login_required
def cadastrarPais(request):
    try:
        pais = Pais()
        pais.nome = request.POST['nome']
        pais.continente = get_object_or_404(Continente, pk=request.POST['continente'])
        pais.save()
    except:
        return HttpResponseRedirect(reverse('cloud:pais'))
    else:
        return HttpResponseRedirect(reverse('cloud:pais'))

@login_required
def toDeletePais(request):
    try:
        pais = get_object_or_404(Pais, id=request.POST['id'])
        pais.delete()
    except:
        return HttpResponseRedirect(reverse('cloud:pais'))
    else:
        return HttpResponseRedirect(reverse('cloud:pais'))


# Cadastro de Região
@login_required
def RegiaoView(request):
    latest_regiao_list = Regiao.objects.order_by('-nome')[:30]
    latest_clima_list = Clima.objects.order_by('nome')
    latest_pais_list = Pais.objects.order_by('nome')

    if len(latest_clima_list) < 1:
        Clima.PopularBanco()
        latest_clima_list = Clima.objects.order_by('nome')

    template = loader.get_template('cloud/cadRegiao.html')
    context = {
        'latest_regiao_list':latest_regiao_list,
        'latest_clima_list':latest_clima_list,
        'latest_pais_list':latest_pais_list,
    }
    return HttpResponse(template.render(context, request))

@login_required
def cadastrarRegiao(request):
    try:
        regiao = Regiao()
        regiao.nome = request.POST['nome']
        regiao.pais = get_object_or_404(Pais, pk=request.POST['pais'])
        regiao.clima = get_object_or_404(Clima, pk=request.POST['clima'])
        regiao.save()
    except:
        return HttpResponseRedirect(reverse('cloud:regiao'))
    else:
        return HttpResponseRedirect(reverse('cloud:regiao'))

@login_required
def toDeleteRegiao(request):
    try:
        regiao = get_object_or_404(Regiao, id=request.POST['id'])
        regiao.delete()
    except:
        return HttpResponseRedirect(reverse('cloud:regiao'))
    else:
        return HttpResponseRedirect(reverse('cloud:regiao'))

# Cadastro de Estado
@login_required
def EstadoView(request):
    latest_estado_list = Estado.objects.order_by('-nome')[:30]
    latest_clima_list = Clima.objects.order_by('nome')
    latest_regiao_list = Regiao.objects.order_by('nome')
    template = loader.get_template('cloud/cadEstado.html')
    context = {
        'latest_estado_list':latest_estado_list,
        'latest_clima_list':latest_clima_list,
        'latest_regiao_list':latest_regiao_list,
    }
    return HttpResponse(template.render(context, request))

@login_required
def cadastrarEstado(request):
    try:
        estado = Estado()
        estado.nome = request.POST['nome']
        estado.regiao = get_object_or_404(Regiao, pk=request.POST['regiao'])
        estado.clima = get_object_or_404(Clima, pk=request.POST['clima'])
        estado.save()
    except:
        return HttpResponseRedirect(reverse('cloud:estado'))
    else:
        return HttpResponseRedirect(reverse('cloud:estado'))

@login_required
def toDeleteEstado(request):
    try:
        estado = get_object_or_404(Estado, id=request.POST['id'])
        estado.delete()
    except:
        return HttpResponseRedirect(reverse('cloud:estado'))
    else:
        return HttpResponseRedirect(reverse('cloud:estado'))

# Cadastro de Cidade
@login_required
def CidadeView(request):
    latest_cidade_list = Cidade.objects.order_by('-nome')[:30]
    latest_clima_list = Clima.objects.order_by('nome')
    latest_estado_list = Estado.objects.order_by('nome')
    template = loader.get_template('cloud/cadCidade.html')
    context = {
        'latest_cidade_list':latest_cidade_list,
        'latest_clima_list':latest_clima_list,
        'latest_estado_list':latest_estado_list,
    }
    return HttpResponse(template.render(context, request))

@login_required
def cadastrarCidade(request):
    try:
        cidade = Cidade()
        cidade.nome = request.POST['nome']
        cidade.estado = get_object_or_404(Estado, pk=request.POST['estado'])
        cidade.clima = get_object_or_404(Clima, pk=request.POST['clima'])
        cidade.save()
    except:
        return HttpResponseRedirect(reverse('cloud:cidade'))
    else:
        return HttpResponseRedirect(reverse('cloud:cidade'))

@login_required
def toDeleteCidade(request):
    try:
        cidade = get_object_or_404(Cidade, id=request.POST['id'])
        cidade.delete()
    except:
        return HttpResponseRedirect(reverse('cloud:cidade'))
    else:
        return HttpResponseRedirect(reverse('cloud:cidade'))