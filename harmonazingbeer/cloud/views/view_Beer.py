from django.shortcuts import get_object_or_404, render, render_to_response
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.template import loader
from django.contrib.auth.decorators import login_required
from ..models.model_Beer import *
from ..models.model_Address import Cidade
from ..models.model_Person import Fabricante, Estabelecimento
from ..sugestao.HarmonizegerarSugestao import *

#Cadastro Tipo
@login_required
def TipoView(request):
    latest_tipo_list = Tipo.objects.order_by('-nome')[:30]
    latest_cor_list = Cor.objects.order_by('nome')
    template = loader.get_template('cloud/cadTipo.html')
    context = {
        'latest_tipo_list':latest_tipo_list,
        'latest_cor_list':latest_cor_list
    }
    return HttpResponse(template.render(context, request))

@login_required
def cadastrarTipo(request):
    try:
        tipo = Tipo()
        tipo.nome = request.POST['nome']
        tipo.cor = get_object_or_404(Cor, pk=request.POST['cor'])
        tipo.save()
    except:
        return HttpResponseRedirect(reverse('cloud:tipo'))
    else:
        return HttpResponseRedirect(reverse('cloud:tipo'))

@login_required
def toDeleteTipo(request):
    try:
        tipo = get_object_or_404(Tipo, id=request.POST['id'])
        tipo.delete()
    except:
        return HttpResponseRedirect(reverse('cloud:tipo'))
    else:
        return HttpResponseRedirect(reverse('cloud:tipo'))

#Cadastro Cor
@login_required
def CorView(request):    
    template =loader.get_template('cloud/cadCor.html')
    latest_cor_list = Cor.objects.order_by('-id')[:30]
    context = {'latest_cor_list':latest_cor_list,}
    return HttpResponse(template.render(context,request))

@login_required
def cadastrarCor(request):
    try:
        cor = Cor()
        cor.nome = request.POST['nome']
        cor.save()
    except:
        return HttpResponseRedirect(reverse('cloud:cor'))
    else:
        return HttpResponseRedirect(reverse('cloud:cor'))

@login_required
def toDeleteCor(request):
    try:
        cor = get_object_or_404(Cor, id=request.POST['id'])
        cor.delete()
    except:
        return HttpResponseRedirect(reverse('cloud:cor'))
    else:
        return HttpResponseRedirect(reverse('cloud:cor'))

#cadastro Rotulo
@login_required
def RotuloView(request):    
    template =loader.get_template('cloud/cadRotulo.html')
    latest_rotulo_list = Rotulo.objects.order_by('nome')[:30]
    latest_cidade_list = Cidade.objects.order_by('nome')
    latest_fabricante_list = Fabricante.objects.order_by('nome')
    latest_tipo_list = Tipo.objects.order_by('nome')
    latest_cor_list = Cor.objects.order_by('nome')

    context = {
        'latest_rotulo_list':latest_rotulo_list,
        'latest_cidade_list':latest_cidade_list,
        'latest_fabricante_list': latest_fabricante_list,
        'latest_tipo_list': latest_tipo_list,
        'latest_cor_list': latest_cor_list,
    }
    return HttpResponse(template.render(context,request))

@login_required
def cadastrarRotulo(request):
    try:
        rotulo = Rotulo()
        rotulo.nome = request.POST['nome']
        rotulo.descricao = request.POST['descricao']        
        rotulo.cidadeFabricada = get_object_or_404(Cidade, pk=request.POST['cidade'])
        rotulo.fabricante = get_object_or_404(Fabricante, pk=request.POST['fabricante'])
        rotulo.tipo = get_object_or_404(Tipo, pk=request.POST['tipo'])
        rotulo.cor = get_object_or_404(Cor, pk=request.POST['cor'])
        rotulo.ibu = request.POST['ibu']
        rotulo.teorAlcoolico = request.POST['teorAlcoolico']        
        rotulo.save()
        SugestaoRotulo.gerarNotaCadastroRotulo(rotulo, request)
    except Exception as e:
        return HttpResponseRedirect(reverse('cloud:rotulo'))
    else:
        return HttpResponseRedirect(reverse('cloud:rotulo'))

@login_required
def toDeleteRotulo(request):
    try:
        rotulo = get_object_or_404(Rotulo, id=request.POST['id'])
        rotulo.delete()
    except:
        return HttpResponseRedirect(reverse('cloud:rotulo'))
    else:
        return HttpResponseRedirect(reverse('cloud:rotulo'))


@login_required
def cadastrarCartaCerveja(request):
    try:
        carta = Carta_Cerveja()
        carta.rotulo = get_object_or_404(Rotulo, pk=request.POST['rotulo'])
        carta.estabelecimento = get_object_or_404(Estabelecimento, pk=request.POST['estabelecimento'])
        carta.save()
    except:
        return HttpResponseRedirect(reverse('cloud:estabelecimento'))
    else:
        return HttpResponseRedirect(reverse('cloud:estabelecimento'))

@login_required
def toDeleteCarta(request):
    try:
        carta = get_object_or_404(Carta_Cerveja, id=request.POST['id'])
        carta.delete()
    except:
        return HttpResponseRedirect(reverse('cloud:estabelecimento'))
    else:
        return HttpResponseRedirect(reverse('cloud:estabelecimento'))