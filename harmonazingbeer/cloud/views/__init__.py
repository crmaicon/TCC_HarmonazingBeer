from .view_general import *
from .view_Beer import *
from .view_Person import *
from .view_Users import *
from .view_Address import *
from .view_Food import *
from .view_Harmonize import *