from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.template import loader
from django.contrib.auth.decorators import login_required


from ..models.model_Harmonize import Sugestao, Nota, PesoCaracteristicas
from ..models.model_Address import Cidade, Pais
from ..models.model_Food import Prato
from ..models.model_Beer import Tipo
from ..models.model_Person import Fabricante
from ..sugestao.HarmonizegerarSugestao import *

@login_required
def sugestaoView(request):
    template =loader.get_template('cloud/gerarSugestao.html')
    latest_prato_list = Prato.objects.order_by('nome')
    latest_cidade_list = Cidade.objects.order_by('nome')
    latest_pais_list = Pais.objects.order_by('nome')
    latest_fabricante_list = Fabricante.objects.order_by('nome')
    latest_tipo_list = Tipo.objects.order_by('nome')
    usuario = get_object_or_404(Usuario, user=request.user.id)
    id_cidade = usuario.cidade.id
    context = {'latest_prato_list':latest_prato_list,
               'latest_cidade_list':latest_cidade_list,
               'latest_pais_list':latest_pais_list,
               'latest_fabricante_list':latest_fabricante_list,
               'latest_tipo_list':latest_tipo_list,
               'id_Cidade_User':id_cidade,
            }
    return HttpResponse(template.render(context,request))

@login_required
def gerarSugestao(request):
    template =loader.get_template('cloud/gerarSugestao.html')
    latest_prato_list = Prato.objects.order_by('nome')
    latest_cidade_list = Cidade.objects.order_by('nome')
    latest_pais_list = Pais.objects.order_by('nome')
    latest_fabricante_list = Fabricante.objects.order_by('nome')
    latest_tipo_list = Tipo.objects.order_by('nome')
    usuario = get_object_or_404(Usuario, user=request.user.id)
    id_cidade = usuario.cidade.id    
    prato = get_object_or_404(Prato, pk=request.POST['prato'])
    id_prato = prato.id
    "aqui buscar a lista de sugestão"
    latest_sugestao_list = SugestaoRotulo.retornarSugestao(request)
    context = {'latest_prato_list':latest_prato_list,
               'latest_cidade_list':latest_cidade_list,
               'latest_sugestao_list':latest_sugestao_list,
               'latest_pais_list':latest_pais_list,
               'latest_fabricante_list':latest_fabricante_list,
               'latest_tipo_list':latest_tipo_list,
               'id_Cidade_User':id_cidade,
               'id_Prato_select':id_prato,
            }
    return HttpResponse(template.render(context,request))

@login_required
def gravarRotuloSelecionado(request):
    try:
        sugestao = Sugestao()        
        sugestao.rotulo = get_object_or_404(Rotulo, pk=request.POST['rotulo'])        
        sugestao.prato = get_object_or_404(Prato, pk=request.POST['prato'])        
        sugestao.usuario = get_object_or_404(Usuario, pk=request.POST['usuario'])        
        sugestao.estacao = get_object_or_404(Estacao, pk=request.POST['estacao'])        
        sugestao.save()
        nota = Nota()
        nota.sugestao = sugestao
        nota.save()
        
    except ValueError:
        return HttpResponseRedirect(reverse('cloud:sugestao'))
    else:
        return HttpResponseRedirect(reverse('cloud:sugestao'))

@login_required
def notaView(request):
    template =loader.get_template('cloud/cadNota.html')    
    latest_nota_list = Nota.objects.filter(sugestao__usuario__user=request.user, nota__isnull=True).order_by('-id')[:30]
    context = {'latest_nota_list':latest_nota_list,}
    return HttpResponse(template.render(context,request))

@login_required
def gravarNota(request):
    try:
        nota = get_object_or_404(Nota, pk=request.POST['id'])        
        nota.nota = request.POST['vlrnota']
        nota.save()
        SugestaoRotulo.gerarMediaNotas()
    except ValueError:
        return HttpResponseRedirect(reverse('cloud:nota'))
    else:
        return HttpResponseRedirect(reverse('cloud:nota'))

@login_required
def pesoView(request):
    template =loader.get_template('cloud/cadPeso.html')    
    latest_Peso = get_object_or_404(PesoCaracteristicas, pk=1)
    context = {'latest_Peso':latest_Peso,}
    return HttpResponse(template.render(context,request))

@login_required
def gravarPeso(request):
    try:
        peso = get_object_or_404(PesoCaracteristicas, pk=1)        
        peso.tipo = request.POST['tipo']
        peso.alcool = request.POST['alcool']
        peso.ibu = request.POST['ibu']
        peso.cor = request.POST['cor']
        peso.fabricante = request.POST['fabricante']
        peso.paisOrigem = request.POST['paisOrigem']
        peso.comida = request.POST['comida']
        peso.variacaoAlcool = request.POST['varAlcool']
        peso.variacaoIBU = request.POST['varIBU']
        peso.save()                
    except ValueError:
        return HttpResponseRedirect(reverse('cloud:peso'))
    else:
        return HttpResponseRedirect(reverse('cloud:peso'))