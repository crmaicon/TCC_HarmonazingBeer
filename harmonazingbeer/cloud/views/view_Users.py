from django.shortcuts import get_object_or_404, render, render_to_response
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone

from django.contrib.auth import authenticate, logout, login as authlogin
from django.template import Context, loader, RequestContext

from ..models.model_Person import *
from ..models.model_Address import Cidade

class UsuarioNewView(generic.ListView):
    model = Usuario
    template_name = 'cloud/cadUsuarioNew.html'
    context_object_name = 'latest_usuario_list'

    def get_queryset(self):
        return Usuario.objects.order_by('-nome')[:5]

    def get_context_data(self, *args, **kwargs):
        context = super(UsuarioNewView, self).get_context_data(*args, **kwargs)
        context['latest_cidade_list'] = Cidade.objects.order_by('nome')    
        return context 

def login(request):
    if (request.user.id):
        return HttpResponseRedirect(reverse('cloud:about'))

    if (request.POST):
        codUser = request.POST['codUsuario']
        senhaUser = request.POST['senhaUsuario'] 
        u = authenticate(username=codUser, password=senhaUser)

        if(u is not None):
            if (u.is_active):
                authlogin(request,u)

                if (request.POST.__contains__('next')):
                    return HttpResponseRedirect(request.POST['next'])
                
                return HttpResponseRedirect(reverse('cloud:about'))
        
        return HttpResponseRedirect(reverse('cloud:index'))

def sair(request):
    logout(request)
    return HttpResponseRedirect(reverse('cloud:index'))
    