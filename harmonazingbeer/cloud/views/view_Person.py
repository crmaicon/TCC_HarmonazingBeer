from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.decorators import login_required

from ..models.model_Person import *
from ..models.model_Address import Cidade
from ..models.model_Beer import Carta_Cerveja, Rotulo

#Cadastro Usuários Logados
@login_required
def UsuarioView(request):    
    template = loader.get_template('cloud/cadUsuario.html')
    latest_usuario_list = Usuario.objects.order_by('-nome')[:5]
    latest_cidade_list = Cidade.objects.order_by('nome')    
    context ={'latest_usuario_list':latest_usuario_list,
              'latest_cidade_list':latest_cidade_list,
    }
    return HttpResponse(template.render(context, request))


def cadastrarUsuario(request):
    try:
        usuario = Usuario()
        usuario.nome = request.POST['nome']
        usuario.codUsuario = request.POST['codUsuario']
        usuario.email = request.POST['email']
        usuario.senha = request.POST['senha']
        usuario.cidade = get_object_or_404(Cidade, pk=request.POST['cidade'])
        usuario.maitre = False 
        if (request.POST.__contains__('maitre')):
            if (request.POST['maitre'] == '1'):            
                usuario.maitre = True
        usuario.save()
    except:
        return HttpResponseRedirect(reverse('cloud:usuario'))
    else:
        return HttpResponseRedirect(reverse('cloud:index'))

@login_required
def toDeleteUsuario(request):
    try:
        usuario = get_object_or_404(Usuario, id=request.POST['id'])
        usuario.delete()
    except:
        return HttpResponseRedirect(reverse('cloud:usuario'))
    else:
        return HttpResponseRedirect(reverse('cloud:usuario'))

#Cadastro Fabricante
@login_required
def FabricanteView(request):
    latest_fabricante_list = Fabricante.objects.order_by('nome')[:30]
    latest_cidade_list = Cidade.objects.order_by('nome')    
    
    template = loader.get_template('cloud/cadFabricante.html')
    context = {
        'latest_fabricante_list': latest_fabricante_list,
        'latest_cidade_list':latest_cidade_list,
    }
    return HttpResponse(template.render(context, request))

@login_required
def cadastrarFabricante(request):
    try:
        fab = Fabricante()
        fab.nome = request.POST['nome']
        fab.cidade = get_object_or_404(Cidade, pk=request.POST['cidade'])        
        fab.save()
    except:
        return HttpResponseRedirect(reverse('cloud:fabricante'))
    else:
        return HttpResponseRedirect(reverse('cloud:fabricante'))

@login_required
def toDeleteFabricante(request):
    try:
        usuario = get_object_or_404(Fabricante, id=request.POST['id'])
        usuario.delete()
    except:
        return HttpResponseRedirect(reverse('cloud:fabricante'))
    else:
        return HttpResponseRedirect(reverse('cloud:fabricante'))

#Cadastro Estabelecimento
@login_required
def EstabelecimentoView(request):    
    usuario = Usuario.objects.filter(codUsuario=request.user.username)
    estabel = Estabelecimento.objects.filter(user = usuario[0])
    if len(estabel) > 0:
        estabel = estabel[0]
    latest_carta_list = Carta_Cerveja.objects.filter(estabelecimento = estabel)
    latest_rotulo_list = Rotulo.objects.order_by('nome')
    latest_cidade_list = Cidade.objects.order_by('nome')
    
    template = loader.get_template('cloud/cadEstabelecimento.html')
    context = {
        'latest_estabelecimento': estabel,
        'latest_carta_list':latest_carta_list,
        'latest_rotulo_list':latest_rotulo_list,
        'latest_cidade_list':latest_cidade_list,
    }
    return HttpResponse(template.render(context, request))

@login_required
def cadastrarEstabelecimento(request):
    try:
        estabel = Estabelecimento()
        estabel.nome = request.POST['nome']
        if (request.POST.__contains__('cidade')):
            estabel.cidade = get_object_or_404(Cidade, pk=request.POST['cidade'])
        estabel.user = get_object_or_404(Usuario, codUsuario=request.user.username)
        estabel.save()
    except:
        return HttpResponseRedirect(reverse('cloud:estabelecimento'))
    else:
        return HttpResponseRedirect(reverse('cloud:estabelecimento'))

@login_required
def toDeleteEstabelecimento(request):
    try:
        estabelecimento = get_object_or_404(Estabelecimento, id=request.POST['id'])
        estabelecimento.delete()
    except:
        return HttpResponseRedirect(reverse('cloud:estabelecimento'))
    else:
        return HttpResponseRedirect(reverse('cloud:estabelecimento'))