from django.shortcuts import get_object_or_404, render, render_to_response
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.views import generic
from django.utils import timezone
from django.contrib.auth.decorators import login_required
from django.http import HttpResponse
from django.template import loader


class IndexView(generic.ListView):
    template_name = 'cloud/index.html'
    def get_queryset(self):
        return ''

class AboutView(generic.ListView):   
    template_name = 'cloud/about.html'
    def get_queryset(self):
        return ''

@login_required
def IndexOnView(request):        
    template = loader.get_template('cloud/indexOn.html')
    context = {}
    return HttpResponse(template.render(context, request))