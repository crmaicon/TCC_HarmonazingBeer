from django.shortcuts import get_object_or_404
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.urls import reverse
from django.template import loader
from django.contrib.auth.decorators import login_required
from ..sugestao.HarmonizegerarSugestao import *
from ..models.model_Food import Comida, Prato, Acompanhamento

#Cadastro Comida
@login_required
def ComidaView(request):    
    template =loader.get_template('cloud/cadComida.html')
    latest_comida_list = Comida.objects.order_by('-id')[:30]
    context = {'latest_comida_list':latest_comida_list,}
    return HttpResponse(template.render(context, request))

@login_required
def cadastrarComida(request):
    try:
        comida = Comida()
        comida.nome = request.POST['nome']
        comida.descricao = request.POST['descricao']
        comida.save()
    except:
        return HttpResponseRedirect(reverse('cloud:comida'))
    else:
        return HttpResponseRedirect(reverse('cloud:comida'))

@login_required
def toDeleteComida(request):
    try:
        comida = get_object_or_404(Comida, id=request.POST['id'])
        comida.delete()
    except:
        return HttpResponseRedirect(reverse('cloud:comida'))
    else:
        return HttpResponseRedirect(reverse('cloud:comida'))

#Cadastro Prato
@login_required
def PratoView(request):    
    template =loader.get_template('cloud/cadPrato.html')
    latest_prato_list = Prato.objects.order_by('-id')[:30]
    latest_comida_list = Comida.objects.order_by('-nome')
    context = {
        'latest_prato_list':latest_prato_list,
        'latest_comida_list':latest_comida_list,
    }
    return HttpResponse(template.render(context, request))

@login_required
def cadastrarPrato(request):
    try:
        prato = Prato()
        prato.nome = request.POST['nome']
        prato.descricao = request.POST['descricao']
        prato.comidaPrincipal = get_object_or_404(Comida, pk=request.POST['comida'])
        prato.save()
        SugestaoRotulo.gerarNotaCadastroPrato(prato, request)
    except:
        return HttpResponseRedirect(reverse('cloud:prato'))
    else:
        return HttpResponseRedirect(reverse('cloud:prato'))

@login_required
def toDeletePrato(request):
    try:
        prato = get_object_or_404(Prato, id=request.POST['id_Prato'])
        prato.delete()
    except:
        return HttpResponseRedirect(reverse('cloud:prato'))
    else:
        return HttpResponseRedirect(reverse('cloud:prato'))

@login_required
def AcompanhamentoView(request):    
    template =loader.get_template('cloud/cadAcompanhamento.html')
    prato = get_object_or_404(Prato, id=request.POST['id_Prato'])    
    latest_comida_list = Comida.objects.order_by('-nome')
    latest_acompanhamento_list = Acompanhamento.objects.filter(prato=request.POST['id_Prato']).order_by('-id')[:30]
    context = {
        'latest_prato':prato,
        'latest_comida_list':latest_comida_list,    
        'latest_acompanhamento_list':latest_acompanhamento_list,
           
    }
    return HttpResponse(template.render(context, request))

@login_required
def cadastrarAcompanhamento(request):
    try:
        acompanhamento = Acompanhamento()
        acompanhamento.prato = get_object_or_404(Prato, id=request.POST['id_Prato'])
        acompanhamento.comida = get_object_or_404(Comida, id=request.POST['id_Comida'])
        acompanhamento.save()

        template =loader.get_template('cloud/cadAcompanhamento.html')
        prato = get_object_or_404(Prato, id=request.POST['id_Prato'])    
        latest_comida_list = Comida.objects.order_by('-nome')
        latest_acompanhamento_list = Acompanhamento.objects.filter(prato=request.POST['id_Prato']).order_by('-id')[:30]
        context = {
            'latest_prato':prato,
            'latest_comida_list':latest_comida_list,
            'latest_acompanhamento_list':latest_acompanhamento_list,        
        }    
    except:
        return HttpResponseRedirect(reverse('cloud:prato'))
    else:
        return HttpResponse(template.render(context, request))

@login_required
def toDeleteAcompanhamento(request):
    try:
        prato = get_object_or_404(Prato, id=request.POST['id'])
        prato.delete()
    except:
        return HttpResponseRedirect(reverse('cloud:prato'))
    else:
        return HttpResponseRedirect(reverse('cloud:prato'))